var $ = jQuery;
$(document).ready(function () {
  init();
});

var tab = null;
var line = 0;
var column = 0;
function init() {
  tab = [
    [1, 0, 1, 1,1], // la grenouille saute la ou il ya les 1 et les transforme apre chaque saut a un zero
    [1, 0, 1, 0,1], // row / line / ligne
    [1, 0, 1, 1,1], // row / line / ligne
    [1, 0, 1, 1,0],
    [1, 1, 1, 0,0]  // row / line / ligne
  ];
gestionTouches();
}

function gestionTouches() {
  var TOP = 38, BOTTOM = 40, LEFT = 37, RIGHT = 39;
  $(document).keydown(function (event) {//la fonction event pour géré les boutton
    var $grenouille = $('.gren');//on recupére img de la grenouille par sa class

    switch (event.keyCode) {

      case BOTTOM:
        MoveGren(line + 1, column);//il se deplace dans les ligne +1
        break;
      case TOP:
        MoveGren(line - 1, column);//il se deplace dans les ligne -1
        break;
      case LEFT:
        MoveGren(line, column - 1);//il se deplace dans les colonnes -1
        break;
      case RIGHT:
        MoveGren(line, column + 1);//il se deplace dans les colonnes +1
        break;
    };
  });
}

function MoveGren(li, co) {
  if (li >= 5) { return; } // limite en bas
  if (li < 0) { return; } // limite en haut
  if (co >= 5) { return; } // limite ver right pour pas aller a l'infini
  if (co < 0) { return; } // limite ver left
  var $grenouille = $('.gren')
  var $destination = $('.col-' + li + '-' + co);
  if ($($destination).hasClass('td-hide') == false) { // Test si case hide
    // HTML
    var $positionActuelle = $('.col-' + line + '-' + column);
    $($positionActuelle).removeClass('td');//enlever la classe td qui est global et la remplacer pour caché le rond
    $($positionActuelle).addClass('td-hide');//.html('<div class="moncercle"><div class="monpetitcercle"></div></div>');//ajoutez hide pour caché les rond marron
    //$($positionActuelle).addClass('tab').html('<div class="moncercle"><div class="monpetitcercle"></div></div>');
    /*setTimeout(function() {
        $($positionActuelle).addClass('tab').html('<div class="moncercle"><div class="monpetitcercle"></div></div>');
    },1000);
    /*clearTimeout(function(){
        $($positionActuelle).addClass('tab').html('<div class="moncercle"></div>').stop();
    });*/
    $destination.prepend($grenouille)//pour insérer l'image de la grenouille
    line = li;
    column = co;

    // JS initialisé mon tableau a chaque fois quil cache le rond marron il lui renitialise a zero
    tab[li][co] = 0;

  }

  gagner();
}
function gagner() {
  var total = 0;
  for (var li in tab) {
    for (var co in tab) {//verification si gagner somme du tab =1

      total += tab[li][co];
    }
  }
  console.log('total', total)
  if (total == 1) {
    setTimeout(redirectionJavascript, 500)// Nb de millisecondes de délai
    function redirectionJavascript() {
      document.location.href = "gagner.html";//redirection vers la page de gagner dans 1 sec
    }
  }
}


